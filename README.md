# runner

This repository contains the Vagrant configuration for our [GitLab CI
runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner). This uses the
same [bootstrap
script](https://gitlab.com/robigalia/devbox/blob/master/bootstrap.sh) from the
devbox to set up Rust for cross-compiling. It also keeps a copy of the target
files from sel4-targets.

sel4-targets polled for changes on every build, but the bootstrap script is
manually updated.
