#!/usr/bin/env bash
set -e

if [ "$(whoami)" = "root" ]; then
	exec sudo -u vagrant -i /vagrant/bootstrap.sh
fi

mkdir .ssh 2>/dev/null || true
echo "gitlab.com,52.21.36.51 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" >> ~/.ssh/known_hosts
echo "gitlab.com,104.210.2.228 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" >> ~/.ssh/known_hosts
sudo sed --in-place /etc/apt/sources.list -e 's/archive.ubuntu.com/mirror.clarkson.edu/g'
sudo sed --in-place /etc/apt/sources.list -e 's/security.ubuntu.com/mirror.clarkson.edu/g'
sudo dpkg --add-architecture i386
sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y
sudo apt-get install -y python python-pip qemu-system libxml2 build-essential \
    git clang gcc-arm-linux-gnueabi gcc-arm-linux-gnueabihf llvm libc6-dev:i386
sudo pip install tempita

if [ ! -e sel4-targets ]; then
    git clone https://gitlab.com/robigalia/sel4-targets.git
fi

sed -i '1i export RUST_TARGET_PATH=/home/vagrant/sel4-targets' ~/.bashrc
source ~/.bashrc

if hash rustc 2>/dev/null; then
    true
else
    curl -sSOf https://static.rust-lang.org/rustup.sh
    sudo bash rustup.sh --yes --channel=nightly --date=2015-12-28 --prefix=/usr
fi
RUSTC_REVISION=$(rustc -vV | awk '/commit-hash/ { print $2; }')
RUSTC_DATE=$(rustc -vV | awk '/commit-date/ { print $2; }')
RUSTC_DIR=~/rust-$RUSTC_DATE/rustc-nightly

if [ ! -e rust-$RUSTC_DATE.stamp ]; then
    mkdir rust-$RUSTC_DATE
    pushd rust-$RUSTC_DATE >/dev/null
    wget --quiet https://static.rust-lang.org/dist/$RUSTC_DATE/rustc-nightly-src.tar.gz
    tar xf rustc-nightly-src.tar.gz
    touch ../rust-$RUSTC_DATE.stamp
    popd >/dev/null
fi

# arg 1 is the rust triple
# arg 2 is the corresponding llvm triple
# arg 3 is the CFLAGS to use when building compiler-rt
# arg 4 is the name of the subdir in compiler-rt's build dir to copy the .a from
make_target_libs() {
    sudo mkdir -p /usr/lib/rustlib/$1/lib
    pushd $RUSTC_DIR >/dev/null
    if [ "$(cat /usr/lib/rustlib/$1/lib/libcompiler-rt.a.stamp 2>/dev/null)" != "$RUSTC_REVISION" ]; then
        pushd src/compiler-rt >/dev/null
        [ -d build ] || mkdir build
        make -j3 CC=clang ProjSrcRoot=$(pwd) ProjObjRoot=$(pwd)/build TargetTriple=$2 "CFLAGS=$3" triple-builtins
        sudo cp build/triple/builtins/$4/libcompiler_rt.a /usr/lib/rustlib/$1/lib/libcompiler-rt.a
        echo $RUSTC_REVISION | sudo tee /usr/lib/rustlib/$1/libcompiler-rt.a.stamp > /dev/null
        popd >/dev/null
    fi
    if [ "$(cat /usr/lib/rustlib/$1/lib/libcore.rlib.stamp 2>/dev/null)" != "$RUSTC_REVISION" ]; then
        pushd src/libcore >/dev/null
        rustc -O --target=$1 lib.rs
        sudo cp libcore.rlib /usr/lib/rustlib/$1/lib
        echo $RUSTC_REVISION | sudo tee /usr/lib/rustlib/$1/lib/libcore.rlib.stamp > /dev/null
        popd >/dev/null
    fi
    popd >/dev/null
}

make_target_libs i686-sel4-unknown i686-unknown-linux-gnu "-target i686-unknown-linux-gnu -I/usr/include/i386-linux-gnu" i386
make_target_libs arm-sel4-gnueabi arm-linux-gnueabi "" arm
make_target_libs arm-sel4-gnueabihf arm-linux-gnueabihf "" arm

/vagrant/setup_runner.sh
